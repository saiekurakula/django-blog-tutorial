
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render

# Create your views here.

from .forms import PostForm
from .models import Posts

def post_create(request):
    form = PostForm(request.POST or None, request.FILES or None)

    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        messages.success(request, "Successfully Created")
        return HttpResponseRedirect(instance.get_absolute_url())

    context = {
        "form": form,        
    }
    return render(request, "post_form.html", context)
    #return HttpResponse("<h1>Create</h1>")


def post_detail(request, id):
#    instance = Posts.objects.get(id=2)
    instance = get_object_or_404(Posts, id=id)
    context = {
        "title": instance.title,
        "instance": instance,
    }

    return render(request, "post_detail.html", context)


def post_list(request):
    queryset_list = Posts.objects.all().order_by("-timestamp")
    paginator = Paginator(queryset_list, 5) # Show 25 queryset per page

    page = request.GET.get('page')
    try:
        queryset = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        queryset = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        
        queryset = paginator.page(paginator.num_pages)
    context = {
        "object_list": queryset,
        "title": "List"
    }
    
    return render(request, "post_list.html", context)
    #return HttpResponse("<h1>List</h1>")


def post_update(request, id=None):
    #return HttpResponse("<h1>Update</h1>")
    instance = get_object_or_404(Posts, id=id)
    form = PostForm(request.POST or None, request.FILES or None, instance=instance)

    if form.is_valid():
        instance = form.save(commit=False)
        instance.save()
        messages.success(request, "Saved")
        return HttpResponseRedirect(instance.get_absolute_url())
        #print form.cleaned_data.get("title")
    
    context = {
        "title": instance.title,
        "instance": instance,
        "form": form,
    }

    return render(request, "post_form.html", context)


def post_delete(request, id=None):
    instance = get_object_or_404(Posts, id=id)
    instance.delete()
    messages.success(request, "Successfully Deleted")
    return redirect("posts: list")
    #return HttpResponseRedirect(insance.get_absolute_url())
    #return HttpResponse("<h1>delete</h1>")


