from django.contrib import admin

# Register your models here.


from .models import Posts

class PostModelAdmin(admin.ModelAdmin):
    list_display = ["title", "updated", "timestamp"]
    # A lot more options at: https://docs.djangoproject.com/en/1.11/ref/contrib/admin/#modeladmin-objects
    class Meta:
        model = Posts

admin.site.register(Posts, PostModelAdmin)
